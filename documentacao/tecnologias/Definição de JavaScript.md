JavaScript é uma linguagem de programação baseada em scripts, utilizada para criar pequenos programas encarregados de realizar ações dentro do âmbito de uma página web.

Trata-se de uma linguagem de programação do lado do cliente, porque é o navegador que suporta a carga de processamento. Graças a sua compatibilidade com a maioria dos navegadores modernos, é a linguagem de programação do lado do cliente mais utilizada.

Com JavaScript é possível criar efeitos especiais nas páginas e definir interatividades com o usuário. O navegador do cliente é o encarregado de interpretar as intruções JavaScript e executá-las para realizar estes efeitos e interatividade, de modo que o maior recurso, e talvez o único, com que conta esta linguagem é o próprio navegador.