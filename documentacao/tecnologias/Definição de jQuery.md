jQuery é uma biblioteca JavaScript poderosa e fácil de utilizar. Essa biblioteca consegue abstrair do desenvolvedor web muito da programação exigida para criação de recursos de interatividade.

O lema do jQuery é uma biblioteca JavaScript que você escreve menos e faz mais.

Dentre os recursos do jQuery estão: seleção e manipulação de elementos HTML, manipulação de CSS, efeitos e animações, navegação pelo DOM, ajax e eventos.

Para utilizá-lo é preciso inserí-lo na página com as tags script. É possível fazer o download do framework no projeto e linkar direto.