JSON que significa JavaScript Object Notation, é um formato leve de troca de informações entre sistemas. Além de ser um formato leve para troca de dados é também simples de ler. Para trabalhar com o JSON e o Java, é necessário utilizar uma biblioteca.

##### Vantagens do JSON
- Leitura mais simples;
- Analisador (parsing) mais fácil;
- Suporta objetos;
- Velocidade maior na execução e transporte de dados;
- Arquivo com tamanho reduzido.