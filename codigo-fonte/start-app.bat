@echo off

start http://localhost:200
node index
if errorlevel 1 (
	echo o comando NPM n�o foi encontrado. Instale o Node.Js (https://nodejs.org/)
	start https://nodejs.org/
	pause
	exit /b %errorlevel%
)
pause